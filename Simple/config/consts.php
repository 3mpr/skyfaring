<?php

# ============================================================================ #
# DIRECTORIES
# ============================================================================ #

define('SIMPLE_ROOT',                     $_SERVER['DOCUMENT_ROOT'] . '/Simple');
define('SYSTEM_ROOT',                                  SIMPLE_ROOT . '/.system');
define('PUBLIC_ROOT',                     $_SERVER['DOCUMENT_ROOT'] . '/public');

define('CONFIG_ROOT',                                   SIMPLE_ROOT . '/config');
define('TEMPLATE_ROOT',                                 SIMPLE_ROOT . '/assets');
define('CSS_ROOT',                                              '../../public/');

# ============================================================================ #
# NAMESPACE CONSTANTS INCLUSION
# ============================================================================ #

require SIMPLE_ROOT . '/Error/consts.php';
require SIMPLE_ROOT . '/HTTP/consts.php';

# ============================================================================ #
# REGULAR EXPRESSIONS
# ============================================================================ #

define('Simple\REGEX_ANY',                                           '([^/]+?)');
define('Simple\REGEX_INT',                                          '([0-9]+?)');
define('Simple\REGEX_ALPHA',                                   '([a-zA-Z_-]+?)');
define('Simple\REGEX_ALPHANUMERIC',                         '([0-9a-zA-Z_-]+?)');
define('Simple\REGEX_STATIC',                                              '%s');
define('Simple\REGEX_IP',           '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$');
define('Simple\REGEX_HOSTNAME',     '^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$');
