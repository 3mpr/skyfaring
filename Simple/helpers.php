<?php

/**
 * Simple recursive function iterating through a given array and giving its max
 * depth to the user.
 */

function array_depth(array $array)
{
    $depth = 1;
    $max_depth = 1;

    foreach($array as $v) {
        (is_array($v) && ($depth = array_depth($v) + 1));
        ($depth > $max_depth && $max_depth = $depth);
    }

    return $max_depth;
}

function in_array_r($needle, $haystack, $strict = false)
{
    debug_backtrace();
    foreach ($haystack as $item)
    {
        if (($strict ? $item === $needle : $item == $needle) 
        || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

function parse_json_file($path)
{
    $string = null;

    if(!$string = file_get_contents($path)) {
        return false;
    }

    $json = null;
    if(empty($json = json_decode($string))) {
        return false;
    }

    return $json;
}

function pretty_dump($data)
{
    print('<pre>'.print_r($data, true).'</pre>');
}

/**
 * Generate a random string, using a cryptographically secure
 * pseudorandom number generator (random_int)
 *
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
 *
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 */
function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[rand(0, $max)];
    }
    return $str;
}

function isset_define($var, $replacement = '')
{
    return (@isset($var) ? $var : $replacement);
}

if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = '';
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(
                    ' ', '-', ucwords(
                        strtolower( str_replace('_', ' ', substr($name, 5)) )
                    )
                )] = $value;
            }
        }

        return $headers;
    }
}
