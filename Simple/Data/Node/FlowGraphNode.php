<?php

namespace Skyfaring\Simple\Data\Node;

class FlowGraphNode extends GraphNode implements FlowGraphNodeInterface
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var integer
     */
    protected $_depth = null;

    /**
     * Class constructor.
     */
    public function __construct($value, $parents = array(), $children = array())
    {
        parent::__construct($value, $parents, $children);
        $this->updateDepth();
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * {@inhertidoc}
     */
    public function getAncestors()
    {
        $ancestors = $this->getAncestorsAndSelf();
        array_shift($ancestors);

        return $ancestors;        
    }

    /**
     * {@inhertidoc}
     */
    public function getAncestorsAndSelf()
    {
        return $this->getAscendance(array(), $this);
    }

    /**
     * Recursive method to check a given node ascendance.
     *
     * @param array $visited The already-visited nodes. The check for a given
     * node presence in the array is recursive
     * @param $node The visited node
     */
    protected function getAscendance(array $visited, FlowGraphNodeInterface $node)
    {
        if ($node->isRoot() || in_array_r($node, $visited))
        {
            return $visited;
        }
        
        $visited[] = $node;

        $parents = $node->getParents();
        foreach ($parents as $parent)
        {
            if(!in_array_r($parent, $visited))
            {
                $visited = $this->getAscendance($visited, $parent);
            }
        }

        return $visited;
    }

    /**
     * {@inhertidoc}
     */
    public function getDescendants()
    {
        $descendants = $getDescendantsAndSelf();
        array_shift($descendants);

        return $descendants; 
    }

    /**
     * {@inhertidoc}
     */
    public function getDescendantsAndSelf()
    {
        return $this->getDescendance(array(), $this);
    }

    /**
     * Recursive method to check a given node descendance.
     *
     * @param array $visited The already-visited nodes. The check for a given
     * node presence in the array is recursive
     * @param $node The visited node
     */
    protected function getDescendance(array $visited, FlowGraphNodeInterface $node, $depth = null)
    {
        if ($node->isLeaf() || in_array_r($node, $visited))
        {
            return $visited;
        }
        
        $visited[] = $node;

        $children = $node->getChildren();
        foreach ($children as $child)
        {
            if(!in_array_r($child, $visited))
            {
                $visited = $this->getDescendance($visited, $child);
            }
        }

        return $visited;
    }

    /**
     * Computes this node depth.
     */
    protected function updateDepth()
    {
        $this->_depth = count($this->getAncestorsAndSelf());
    }

    /**
     * {@inheritdoc}
     */
    public function getDepth()
    {
        return $this->_depth;
    }

    /**
     * {@inheritdoc}
     */
    public function addParent(NodeInterface $parent)
    {
        parent::addParent($parent);
        $this->updateDepth();
    }

    /**
     * {@inheritdoc}
     */
    public function removeParent(NodeInterface $parent)
    {
        foreach ($this->_parents as $key => $thisParent)
        {
            if ($parent == $thisParent) 
            {
                unset($this->_parents[$key]);
            }
        }

        $this->updateDepth();

        return $parent;
    }

    /**
     * {@inheritdoc}
     */
    public function removeParents()
    {
        $parents = $this->_parents;
        $this->_parents = array();

        $this->updateDepth();

        return $parents;
    }

    /**
     * {@inheritdoc}
     */
    public function setParents(array $parents)
    {
        foreach($this->_parents as $parent)
        {
            $parent->removeChild($this);
        }

        $oldParents = $this->_parents;
        $this->_parents = array();

        foreach($parents as $parent)
        {
            $this->addParent($parent);
        }

        $this->updateDepth();

        return $oldParents;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray($array = array(), $depth = null)
    {
        $depth = empty($depth) ? $this->_depth : $depth;
        
        // !!! Makes everything buggish
        //
        // if($this->_depth < $depth)
        // { 
        //     return '@'.$this->_name;
        // }

        $retval = $array;

        if(!empty($this->_value))
        {
            if($this->isLeaf())
            {
                $retval[$this->_name] = $this->_value;
            } 
            
            else
            {
                $retval[$this->_name]['value'] = $this->_value;
            }
        }

        foreach($this->_children as $child)
        {
            $retval[$this->_name] = $child->toArray(@$retval[$this->_name], $depth);
        }

        return $retval;
    }
}