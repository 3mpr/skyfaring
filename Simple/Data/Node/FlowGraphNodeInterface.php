<?php

namespace Skyfaring\Simple\Data\Node;

interface FlowGraphNodeInterface extends GraphNodeInterface
{
    /**
     * Retreives all the ancestors.
     *
     * @return mixed
     * @see getDescendants
     */
    public function getAncestors();

    /**
     * Retreives all the ancestors and self.
     *
     * @return mixed
     * @see getDescendantsAndSelf
     */
    public function getAncestorsAndSelf();

    /**
     * Retreives all the descendants.
     *
     * @return mixed
     * @see getAncestors
     */
    public function getDescendants();

    /**
     * Retreives all the descendants and self.
     *
     * @return mixed
     * @see getAncestorsAndSelf
     */
    public function getDescendantsAndSelf();

    /**
     * Gets the current node depth relative to the closest root.
     *
     * @return integer The depth
     */
    public function getDepth();

    /**
     * Returns an array representation of this configuration.
     *
     * @return array The array Representation
     */
    public function toArray($array = array(), $depth = null);
}