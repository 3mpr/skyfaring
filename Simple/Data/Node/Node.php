<?php

namespace Skyfaring\Simple\Data\Node;

/**
 * All-purposes Node definition.
 *
 * Implements NodeInterface only, thus abstract.
 */
abstract class Node implements NodeInterface
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var mixed
     */
    protected $_value = null;

    /**
     * @var array
     */
    protected $_children = array();

    /**
     * Class constructor.
     */
    public function __construct($value)
    {
        $this->_value = $value;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * {@inheritdoc}
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * {@inheritdoc}
     */
    public function isLeaf()
    {
        return 0 == count($this);
    }
    
    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->_children);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->_children);
    }

/* ========================================================================== */
/* === ABSTRACT METHODS ===================================================== */
/* ========================================================================== */

    /**
     * {@inheritdoc}
     */
    public abstract function addChild(NodeInterface $child);

    /**
     * {@inheritdoc}
     */
    public abstract function removeChild(NodeInterface $child);

    /**
     * {@inheritdoc}
     */
    public abstract function removeChildren();

    /**
     * {@inheritdoc}
     */
    public abstract function setChildren(array $children);

    /**
     * {@inheritdoc}
     */
    public abstract function getChildren();

    /**
     * {@inheritdoc}
     */
    public abstract function getNeighbors();

    /**
     * {@inheritdoc}
     */
    public abstract function getNeighborsAndSelf();
}