<?php

namespace Skyfaring\Simple\Data\Config;

use Skyfaring\Simple\Data\Node as SNode;

class Parser
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Blueprint
     */
    protected $_blueprint = null;

    /**
     * @var array
     */
    protected $_store = array();

    /**
     * Class constructor.
     *
     * @param Schema $blueprint The blueprint policy this Parser has to verify
     * @param array $store
     */
    public function __construct(Blueprint $blueprint = null, array $store = array())
    {
        $this->_blueprint = $blueprint;
        $this->_store = $store;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Parse the given file to the correct format.
     *
     * @param string $file The file to parse
     * @return mixed The parsed array | unserialized file
     */
    public function parse($path)
    {
        if (!file_exists($path))
        {
            throw new \DomainException(
                'File not found at '.$path,
                901
            );
        }

        $parts = explode('.', $path);
        $extension = $parts[count($parts) - 1];

        switch ($extension)
        {
            case 'yaml':
            case 'yml':

                $callback = 'yaml_parse_file';

                break;
            
            case 'ini':

                $callback = 'parse_ini_file';

                break;

            case 'json':

                $callback = 'parse_json_file';

                break;
            
            case 'back':
            case 'save':

                $callback = '@unserialize';

                break;
            
            # Add configuration fallback here.

            default:

                throw new \RuntimeException(
                    'Data type .'.$extension.' is not implemented.',
                    902
                );

                break;
        }

        $file = $callback($path);

        if (empty($file))
        {
            throw new \RuntimeException
            (
                'Error attempting to parse '.$file.'.',
                903
            );
        }

        return $file;

        // if (!empty($this->_blueprint) && !$this->_blueprint->match($config))
        // {
        //     throw new RuntimeException
        //     (
        //         'File '.$file.' failed to succeed blueprint test.',
        //         902
        //     );
        // }
    }
}
