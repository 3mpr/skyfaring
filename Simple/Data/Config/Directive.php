<?php

namespace Skyfaring\Simple\Data\Config;

use Skyfaring\Simple\Data\Node as SNode;

/**
 * Configuration directive representation.
 * Holds a value and linked children directives.
 *
 * @see FlowGraphNodeInterface
 */
class Directive extends SNode\FlowGraphNode
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Directive
     */
    protected $_root = null;

    /**
     * @var string
     */
    protected $_name = null;

    /**
     * Class constructor.
     *
     * @param string    $name      The name of this directive
     * @param Config    $config    The configuration this directive lives in
     * @param array     $parents   This directive parents directives
     * @param array     $children  This directive children directives
     * 
     * {@inheritdoc}
     */
    public function __construct($name, $root = null, $value = null, 
        $parents = array(), $children = array())
    {
        $this->setName($name);
        
        parent::__construct($value, $parents, $children);
        
        if(empty($this->_parents))
        {
            $root = $this;
        }
        
        else
        {
            $root = ($this->_parents[key($this->_parents)]->getRoot());
        }

        $this->_root = $root;
    }

    /**
     * Returns this node value as a string.
     */
    public function __toString()
    {
        return (string) $this->_value;
    }
    
/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Tiny factory.
     * Parses an array into a directive network in a recursive manner.
     * Resolves annotations.
     *
     * @param string    $key     This directive name
     * @param Directive $root    This directive root directive
     * @param mixed     $value   This directive value or - if is array - children
     * @param array     $parents The parents
     */
    public static function create($key, $root = null, $value = null, array $parents = array())
    {
        $directive = new Directive($key, $root, null, $parents);
        if (empty($root)) $root = $directive->getRoot();

        if (is_array($value))
        {
            foreach($value as $name => $node)
            {
                $directive->addChild( self::create($name, $root, $node, array($directive)) );
            }
        } 
        
        elseif(substr($value, 0, 1) == self::ANNOTATION)
        {
            $seeked = ltrim($value, self::ANNOTATION);
            if (false != $found = $directive->getRoot()->seek($seeked))
            {
                $directive->setChildren($found->getChildren());
                $directive->setValue($found->getValue());
            }

            else
            {
                throw new \Exception(
                    'Unable to parse annotation '.$seeked.' for '.$key.'.',
                    907
                );
            }
        } 
        
        else
        {
            $directive->setValue($value);
        }

        return $directive;
    }

    /**
     * Sets this Directive name.
     *
     * @param string|integer $name The name
     * @throws \InvalidArgumentException for invalid name types
     */
    public function setName($name)
    {
        if(!(is_string($name) || is_integer($name)))
        {
            throw new \InvalidArgumentException(
                'Directive name must be string or integer, '.$name.' given.',
                905
            );
        }

        $this->_name = $name;
    }

    /**
     * Retreives this Directive name.
     *
     * @return string The name
     */
    public function getName()
    {
        return $this->_name;
    }    

    /**
     * Retreives this directive root.
     */
    public function getRoot()
    {
        return $this->_root;
    }

    /**
     * Recursively seeks for a given node value in a direction or the other
     * from this node. Direction = true means this method will seek in children,
     * Direction = false means this method will seek in parents.
     *
     * @param string $value The seeked value.
     * @param boolean $direction Wheter this method should seek in children (true)
     * or in parents (false).
     *
     * @return Directive The found directive or false on failure. 
     */
    public function seek($name, $direction = true)
    {
        $found = false;

        if ($direction)
        {
            return $this->seekIn($this, $name);
        } 
        
        $array = $this->getAncestorsAndSelf();
        foreach($array as $directive)
        {
            if( false != $found = $this->seekIn($directive, $name) )
            {
                return $found;
            }
        }

        return $found;
    }

    /**
     * Recursivly searchs in a given directive for a value.
     *
     * @param Directive $directive The Directive to recursively search in.
     * @param string $name The seeked value.
     */
    protected function seekIn(Directive $directive, $name)
    {
        foreach ($directive->getChildren() as $child)
        {
            if(is_integer($child->getName()))
            {
                continue 1;
            }

            if($name == $child->getName())
            {
                return $child;
            } 
            
            if(!$child->isLeaf())
            {
                if(false !== $found = $this->seekIn($child, $name))
                {
                    return $found;;
                }
            }
            
        }

        return false;
    }
}