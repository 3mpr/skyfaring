<?php

namespace Skyfaring\Simple\MVC\View;

use Skyfaring\Simple as S;
use Skyfaring\Simple\Templating as SimpleTemplating;

/**
 * This class is the third part of the MVC pattern.
 * This is the representation layer of the application and SHOULD therefore
 * implements any given method to ease content display.
 *
 * @author eka
 *
 * @TODO Implements HIJAX logic - 
 * @TODO Make this class useful (kind of a pipeline for now)
 */
class View
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected $_config = null;

    /**
     * @var Skyfaring\Simple\Templating\Document
     */
    protected $_webpage = null;

    /**
     * Class constructor.
     *
     * Defines view values from the corresponding named block within the
     * "views.yml" file. Any non-defined value within the foresaid block is
     * replaced by the 'default' block value.
     *
     * @param string $page The page name aka yaml section to take attributes from
     */
    final public function __construct($page = 'default')
    {
        $this->_config = S\App::getConfiguration()->get('routes.'.$page.'.content');

        if (empty($this->_config))
        {
            throw new \RuntimeException(
                'Configuration directive for '.$page.' does not exist.',
                1110
            );
        }  

        $this->_webpage = new SimpleTemplating\Document($this->_config->getRoot());
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Retreives this View document.
     */
    public function getDocument()
    {
        return $this->_webpage;
    }

    /**
     * {@inheritdoc}
     */
    public function compose()
    {
        return $this->_webpage->compose();
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        return $this->_webpage->render();
    }

    /**
     * Adds content to the document.
     * The active content is used if no content name is provided
     *
     * @param array  $matter   The added content
     * @param string $content  The content name to add values to
     */
    public function add(array $matter, $content = null)
    {
        if (array_depth($matter) != 1)
        {
            throw new \InvalidArgumentException(
                'Invalid document values given to View.',
                1102
            );
        }

        $design = $this->_webpage->getDesign($content);

        if(empty($design))
        {
            throw new \RuntimeException(
                'Content <i>"'.$content.'"</i> does not exist in document',
                1103
            );
        }

        foreach ($matter as $key => $value) 
        {
            $design->set($key, $value);
        }
    }
}
