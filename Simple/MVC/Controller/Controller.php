<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple\PSR as PSR;
use Skyfaring\Simple\MVC\Model as SModel;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\HTTP as SHTTP;

abstract class Controller
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Model
     */
    protected $_model = null;

    /**
     * @var View
     */
    protected $_view = null;

    /**
     * Class constructor.
     */
    public function __construct(SModel\Model $model = null, SView\View $view = null)
    {
        $this->_model = isset($model) ? $model : new SModel\Model();
        $this->_view = isset($view) ? $view : new SView\View();
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Tiny factory. For fluent behaviour.
     */
    static public function create(SModel\Model $model = null, SView\View $view = null)
    {
        return new static($model, $view);
    }

    /**
     * Sends a response back to the client.
     */
    public function respond(PSR\StreamInterface $stream = null, array $headers = array(), $statusCode = '200')
    {
        if (is_null($stream)) {
            $stream = $this->_view->compose();
        }

        SHTTP\Response::create($stream, $headers, $statusCode)->send();
    }

    /**
     * This method is the method called if none is provided within the route
     * definition and must therefore be implemented in child controller classes.
     *
     * @param array $params The parameters to work with.
     */
    abstract public function run(array $params = array());

}
