<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\Templating as STemplating;

class ConfigController extends Controller
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('skf-config'));

        $header = STemplating\Design::component(
            'skf/skf-header.template.html',
            array('PATH' => '/config') // TODO - Dynamise
        );

        $this->_view->getDocument()->addComponent('header', $header);
    }

    public function index()
    {
        $appConfig = S\App::getConfiguration();
        $document = $this->_view->getDocument();
        $layout = $document->getDesign('content');

        $infos = $document->getDesign('general');

        $infos->set('version', $appConfig->get('version')->toString());
        $infos->set('environment', $appConfig->get('environment')->toString());
        
        $layout->set('content', $infos);
        $document->draft('content');
        $this->respond();
    }

    public function run(array $params = array())
    {
        $this->index();
    }
}
