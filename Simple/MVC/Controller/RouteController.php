<?php

namespace Skyfaring\Simple\MVC\Controller;

use Skyfaring\Simple as S;
use Skyfaring\Simple\MVC\View as SView;
use Skyfaring\Simple\Templating as STemplating;

class RouteController extends Controller
{
    public function __construct()
    {
        parent::__construct(null, new SView\View('skf-route'));

        $header = STemplating\Design::component(
            'skf/skf-header.template.html',
            array('PATH' => '/config/route') // TODO - Dynamise
        );

        $this->_view->getDocument()->addComponent('header', $header);
    }

    public function run(array $params = array())
    {
        $this->listRoutes();
    }

    public function listRoutes()
    {
        $defRoutes = S\App::getRouter()->getRoutes()->all();
        $document  = $this->_view->getDocument();

        $routesTpl = $document->getDesign('list');

        $routeTplSet = STemplating\Design::componentSet(
            $document->getDesign('route')
        );

        foreach ($defRoutes as $route) {

            $routeMethods = $route->getAvailableMethods();
            $routeCtrlWithNamespace = $route->getController();
            $routeCtrlArray = explode('\\', $routeCtrlWithNamespace);
            $routeCtrl = $routeCtrlArray[count($routeCtrlArray) - 1];

            $routeTplSet->addArray(array(
                'NAME'              => $route->getName(),
                'PATH'              => $route->getPath(),
                'CONTROLLER'        => $routeCtrl,
                'ACTION'            => $route->getAction(),
                'GET_CHECKED'       => in_array('GET', $routeMethods) ? 'checked' : '',
                'PUT_CHECKED'       => in_array('PUT', $routeMethods) ? 'checked' : '',
                'POST_CHECKED'      => in_array('POST', $routeMethods) ? 'checked' : '',
                'DELETE_CHECKED'    => in_array('DELETE', $routeMethods) ? 'checked' : ''
            ));
        }

        $routesTpl->set('ROUTES', $routeTplSet);
        $routesTpl->set('NBROUTE',(string) count($defRoutes));
        $document->getDesign('content')->set('content', $routesTpl);

        $document->draft('content');
        $this->respond();
    }
}
