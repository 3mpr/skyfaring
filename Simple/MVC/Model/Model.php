<?php

namespace Skyfaring\Simple\MVC\Model;

use Skyfaring\Simple as s;

/**
 * This class is the second part of the MVC pattern.
 * This is where the logic of the application (Database calls, encryption, ... )
 * SHOULD take place.
 *
 * @author eka
 */
class Model
{
/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * INI class instance. Represents any given - mainline - parameters.
     * This var is declared as static hence shared with every Model.
     *
     * @var INI
     *
     * @see Skyfaring\Simple\INI
     */
    protected static $ini = null;

    /**
     * DB class instance. Represents a connection with a given database.
     *
     * @var DB
     *
     * @see Skyfaring\Simple\DB
     */
    protected static $database = null;

    /**
     * Class constructor.
     * For this constructor to success, a config.ini file MUST be available at
     * Simple/config/config.ini.
     */
    public function __construct()
    {
        if (self::$ini === null || self::$database === null) {
            self::$ini = new s\INI(SIMPLE_ROOT.'/config/database.ini');

            $host = self::$ini->get('host', 'database');
            $user = self::$ini->get('user', 'database');
            $pass = self::$ini->get('password', 'database');
            $schema = self::$ini->get('schema', 'database');

            // self::$database = new s\DB($host, $user, $pass, $schema);
        }
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * This method allows to override the config file.
     *
     * @param INI OR string
     */
    public function setConfig($config)
    {
        if ($config instanceof s\INI) {
            self::$ini = $config;
        } else {
            self::$ini = new s\INI($config);
        }
    }

    /**
     * Simply wraps the constructor call.
     * Should be used after the configuration file replacement.
     */
    public function reload()
    {
        __construct();
    }
}
