<?php

namespace Skyfaring\Simple\HTTP;

use Skyfaring\Simple as S;
use Skyfaring\Simple\PSR as PSR;

class Uri implements PSR\UriInterface
{
    protected static $_supportedSchemes = ['http', 'https'];
    protected static $_supportedPorts = [80, 443];

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Collection
     */
    protected $_server = null;

    /**
     * @var Collection
     */
    protected $_uriComponents = null;

    /**
     * @var Collection
     */
    protected $_query = null;

    /**
     * Class constructor.
     *
     * @param array $uriComponents The uri array following parse_url() format
     * @param array $server        Server-wide ($_SERVER) format array
     */
    public function __construct(array $components, array $server = array(), array $query = array())
    {
        $this->_uriComponents = new S\Data\Collection($components);
        $this->_server = new S\Data\Collection($server);
        $this->_query = new S\Data\Collection($query);
    }

    /**
     * Clone redifinition to avoid shallow copy.
     */
    public function __clone()
    {
        $this->_server = clone $this->_server;
        $this->_uriComponents = clone $this->_uriComponents;
        $this->_query = clone $this->_query;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        $uriString = '';
        $uriBag = $this->_uriComponents;
        $uriComponents = ['scheme', 'user', 'host', 'port', 'path', 'query', 'fragment'];

        foreach ($uriComponents as $component) {
            if ($uriBag->has($component)) {
                switch ($component) {
                    case 'scheme':
                        $uriString .= $uriBag->get('scheme').'://';
                        break;

                    case 'user':
                        $uriString .= $uriBag->get('user').($uriBag->has('pass')
                            ? ':'.$uriBag->get('pass')
                            : ''
                        ).'@';
                        break;

                    case 'host':
                        $uriString .= $uriBag->get('host');
                        break;

                    case 'port':
                        if (!in_array($uriBag->get('port'), self::$_supportedPorts)) {
                            $uriString .= ':'.$uriBag->get('port');
                        }
                        break;

                    case 'path':
                        $uriString .= $uriBag->get('path');
                        break;

                    case 'query':
                        $uriString .= '?'.$uriBag->get('query');
                        break;

                    case 'fragment':
                        $uriString .= '#'.$uriBag->get('fragment');
                        break;
                }
            }
        }

        return $uriString;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Sets the scheme.
     * A(n) empty|null|false scheme will delete the current scheme.
     *
     * @param string $scheme The new scheme if any
     *
     * @return this For chaining
     *
     * @throws \InvalidArgumentException For unsupported schemes
     */
    public function setScheme($scheme)
    {
        $scheme = strtolower($scheme);

        if (in_array($scheme, array('', null, false))) {
            $this->_uriComponents->remove('scheme');
        } elseif (in_array($scheme, self::$_supportedSchemes)) {
            $this->_uriComponents->set('scheme', $scheme);

            if ('https' === $scheme) {
                $this->_server->set('HTTPS', 'on');
                $this->_server->set('SERVER_PORT', 443);
            } else {
                $this->_server->remove('HTTPS');
                $this->_server->set('SERVER_PORT', 80);
            }
        } else {
            throw new \InvalidArgumentException(
                'Unsupported scheme '.$scheme.'.',
                1201
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getScheme()
    {
        return $this->_components->get('scheme', $this->isSecure() ? 'https' : 'http');
    }

    /**
     * {@inheritdoc}
     */
    public function withScheme($scheme)
    {
        $uri = clone $this;

        return $uri->setScheme($scheme);
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthority()
    {
        return $this->_server->get('HTTP_HOST', '');
    }

    /**
     * Sets user name and password.
     *
     * @param string $user The user name
     * @param string $name The user password
     *
     * @return this For chaining
     */
    public function setUserInfo($name, $password = null)
    {
        $this->_uriComponents->set('user', $name);
        isset($password) && $this->_uriComponents->set('pass', $password)
        || $this->_uriComponents->remove('pass');

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserInfo()
    {
        $user = $this->_uriComponents->get('user', '');
        $password = $this->_uriComponents->get('password', '');

        return $user.($password !== '' ? ':'.$password : '');
    }

    /**
     * {@inheritdoc}
     */
    public function withUserInfo($user, $password = null)
    {
        $uri = clone $this;

        return $uri->setUserInfo($user, $password);
    }

    /**
     * Sets the host.
     * A(n) empty|null|false scheme will delete the current host.
     *
     * @param string $host The host
     *
     * @return this For chaining
     *
     * @throws \InvalidArgumentException for invalid hostnames
     */
    public function setHost($host)
    {
        if (in_array($host, array('', null, false))) {
            $this->_uriComponents->remove('host');
        } elseif (preg_match('/'.Simple\REGEX_HOSTNAME.'/', $host) || preg_match('/'.Simple\REGEX_IP.'/', $host)) {
            $this->_uriComponents->set('host', $host);
        } else {
            throw new \InvalidArgumentException(
                'Invalid hostname '.$host.'.',
                1202
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHost()
    {
        return strtolower($this->_uriComponents->get('host', ''));
    }

    /**
     * {@inheritdoc}
     */
    public function withHost($host)
    {
        $uri = clone $this;

        return $uri->setHost($host);
    }

    /**
     * Sets the port.
     *
     * @param int $port The new port
     *
     * @return this For chaining
     *
     * @throws \InvalidArgumentException for invalid ports
     */
    public function setPort($port)
    {
        if (in_array($port, array('', null, false))) {
            $this->_uriComponents->remove('port');
            $this->_server->remove('SERVER_PORT');
        } elseif (in_array($port, self::$_supportedPorts) || ($port > 1000 && $port < 65536)) {
            $this->_uriComponents->set('port', $port);
            $this->_server->set('SERVER_PORT', $port);
        } else {
            throw new \InvalidArgumentException(
                'Port '.$port.'is not supported.',
                1203
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPort()
    {
        if (in_array($this->_uriComponents->get('port'), array(80, 443))) {
            return null;
        }

        return $this->_uriComponents->get('port');
    }

    /**
     * {@inheritdoc}
     */
    public function withPort($port)
    {
        $uri = clone $this;

        return $uri->setPort($port);
    }

    /**
     * Sets the path.
     * If the given path does not begin with a trailing slash, the new path is
     * considered relative to the old one.
     *
     * @param string $path The new path
     *
     * @return this For chaining
     *
     * @throws \InvalidArgumentException for invalid paths
     */
    public function setPath($path)
    {
        if (in_array($path, array('', '/', null, false))) {
            $this->_uriComponents->set('path', '/');
        } elseif (preg_match('/^[^*?"<>|:]*$/', $path)) {

            // If the path is absolute
            if (preg_match('/^/.*/')) {
                $this->_uriComponents->set('path', $path);
            } else {
                $oldPath = $this->_uriComponents->get('path', '/');
                $this->_uriComponents->set('path', $oldPath.'/'.$path);
            }
        } else {
            throw new \InvalidArgumentException(
                'Invalid path '.$path.'.',
                1204
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
        return $this->_uriComponents->get('path', '');
    }

    /**
     * {@inheritdoc}
     */
    public function withPath($path)
    {
        $uri = clone $this;

        return $uri->setPath($path);
    }

    /**
     * Sets the query string.
     *
     * @param string query The query string
     *
     * @return this For chaining
     *
     * @throws \InvalidArgumentException for invalid query strings
     *                                   TODO - Checks if its k
     */
    public function setQuery($query)
    {
        if (in_array($query, array('', null, false))) {
            $this->_uriComponents->remove('query');
        } elseif (preg_match('/([\w-]+(=[\w-]*)?(&[\w-]+(=[\w-]*)?)*)?$/')) {
            $this->_uriComponents->set('query', $query);
        } else {
            throw new \InvalidArgumentException(
                'Invalid query string ['.$query.'].',
                1205
            );
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return $this->_uriComponents->get('query', '');
    }

    /**
     * {@inheritdoc}
     */
    public function withQuery($query)
    {
        $uri = clone $this;

        return $uri->setQuery($query);
    }

    /**
     * Returns the $_query Collection.
     *
     * @return Collection The query Parameter bag
     */
    public function getQueryBag()
    {
        return $this->_query;
    }

    /**
     * Sets the fragment.
     *
     * @param string $fragment The fragment
     *
     * @return this For chaining
     */
    public function setFragment($fragment)
    {
        if (in_array($fragment, array('', null, false))) {
            $this->_uriComponents->remove('fragment');
        } else {
            $this->_uriComponents->set('fragment', urlencode($fragment));
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFragment()
    {
        return $this->_uriComponents->get('fragment', '');
    }

    /**
     * {@inheritdoc}
     */
    public function withFragment($fragment)
    {
        $uri = clone $this;

        return $uri->setFragment($fragment);
    }

    /**
     * Checks wheter the connection is secured or not.
     *
     * @return bool
     */
    public function isSecure()
    {
        return ($this->_server->has('HTTPS') && strtolower($this->_server->get('HTTPS')) !== 'off')
               || $this->_server->get('SERVER_PORT') == 443;
    }
}
