<?php

namespace Skyfaring\Simple\HTTP;

use Skyfaring\Simple\Stream as SimpleStream;
use Skyfaring\Simple\PSR as PSR;

/**
 * {@inheritdoc}
 */
abstract class Message implements PSR\MessageInterface
{
    protected static $_httpStatus = null;

    const HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_HEAD = 'HEAD';
    const HTTP_METHOD_POST = 'POST';
    const HTTP_METHOD_PUT = 'PUT';
    const HTTP_METHOD_DELETE = 'DELETE';
    const HTTP_METHOD_CONNECT = 'CONNECT';
    const HTTP_METHOD_OPTIONS = 'OPTIONS';
    const HTTP_METHOD_TRACE = 'TRACE';

    protected static $_methods = [
        'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE',
    ];

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var string
     */
    protected $_protocolVersion = PROTOCOL_VERSION;

    /**
     * @var array
     */
    protected $_headers = [];

    /**
     * @var StreamInterface
     */
    protected $_body = null;

    /**
     * Class constructor.
     *
     * @param StreamInterface $body    This message body
     * @param array           $headers Headers array
     */
    public function __construct(PSR\StreamInterface $body, array $headers = null)
    {
        self::initialize();

        $this->_body = $body;
        $this->_headers = $headers;
    }

    /**
     * Class destructor.
     *
     * Unsets intern Stream as a message body should not withstand its Message
     * parent object.
     */
    public function __destruct()
    {
        unset($this->_body);
    }

    /**
     * Redefines 'clone' to avoid shallow copies of this object member viariables.
     */
    public function __clone()
    {
        $this->_body = clone $this->_body;
    }


/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Establishes class-wide HTTP status values from the HTTP_STATUS_MAP
     * serialized constant.
     */
    public static function initialize()
    {
        if (!isset(self::$_httpStatus)) {
            self::$_httpStatus = unserialize(HTTP_STATUS_MAP);
        }
    }

    /**
     * Sets the http protocol version.
     */
    public function setProtocolVersion($version)
    {
        $this->_protocolVersion = $version;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProtocolVersion()
    {
        return $this->_protocolVersion;
    }

    /**
     * {@inheritdoc}
     */
    public function withProtocolVersion($version)
    {
        $message = clone $this;

        return $message->setProtocolVersion($version);
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * {@inheritdoc}
     */
    public function hasHeader($header)
    {
        if (isset($this->_headers[strtolower($header)])) {
            return true;
        }

        return false;
    }

    /**
     * Sets the headers.
     *
     * @param array $headers The header array
     * @param bool  $replace "Erase old values" trigger
     */
    public function setHeaders($headers, $replace = false)
    {
        if ($replace) {
            $this->_headers = array();
        }

        foreach ($headers as $header => $value) {
            $this->addHeader($header, $value);
        }
    }

    /**
     * Adds a header to this message header list.
     *
     * @param string $name    The header name
     * @param string $value   The header value
     * @param bool   $replace "Erase old values" trigger
     *
     * @throws \InvalidArgumentException for invalid header name / value
     *
     * This method implements a fluent interface.
     */
    public function addHeader($name, $value, $replace = false)
    {
        if (0 == preg_match('/[a-zA-Z-]*/i', $name)) {
            throw \InvalidArgumentException(
                'Header '.$name.' is not valid.',
                1206
            );
        }

        $replace && $this->_headers[strtolower($name)] = array();
        $this->_headers[strtolower($name)][] = $value;

        return $this;
    }

    /**
     * addHeader($name, $value, true) shorthand.
     */
    public function setHeader($name, $value)
    {
        return $this->addHeader($name, $value, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getHeader($header)
    {
        if (!isset($this->_headers[strtolower($header)])) {
            return array();
        }

        return $this->_headers[strtolower($header)];
    }

    /**
     * Deletes the given header.
     *
     * @param string $name The header name
     *
     * This method implements a fluent interface.
     */
    public function deleteHeader($name)
    {
        if (isset($this->_headers[strtolower($name)])) {
            unset($this->_headers[$name]);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeaderLine($header)
    {
        if (isset($this->_headers[strtolower($header)])) {
            return '';
        }

        $string = '';
        foreach ($this->_headers[strtolower($header)] as $k => $v) {
            $string .= $v.',';
        }

        return $string;
    }

    /**
     * {@inheritdoc}
     */
    public function withHeader($name, $value)
    {
        $message = clone $this;

        return $message->setHeader($name, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function withAddedHeader($name, $value)
    {
        $message = clone $this;

        return $message->addHeader($name, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function withoutHeader($name)
    {
        $message = clone $this;

        return $message->deleteHeader($name);
    }

    /**
     * Sends the specified header if present.
     */
    public function sendHeader($name)
    {
        if (isset($this->_headers[$name])) {
            $cpt = 0;
            foreach ($this->_headers[$name] as $header) {
                (0 == $cpt && $erase = true) || $erase = false;
                header($name.': '.$header, $erase);
                ++$cpt;
            }
        }
    }

    /**
     * Sends every header this message has.
     */
    public function sendHeaders()
    {
        foreach ($this->_headers as $key => $value) {
            $this->sendHeader($key);
        }
    }

    /**
     * Destroys old body and set a new one.
     *
     * @param StreamInterface $body The new body
     *
     * This method implements a fluent interface.
     */
    public function setBody(SimpleStream\StreamInterface $body)
    {
        unset($this->_body);
        $this->_body = $body;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * {@inheritdoc}
     */
    public function withBody(SimpleStream\StreamInterface $body)
    {
        $message = clone $this;

        return $message->setBody($body);
    }
}
