<?php

# ============================================================================ #
# HTTP STATUS LINE => NUMBER
# ============================================================================ #

define('Skyfaring\Simple\HTTP\PROTOCOL_VERSION',                          '1.1');

# Informal & Warnings
# http://www.iana.org/assignments/http-warn-codes/http-warn-codes.xhtml

define('Skyfaring\Simple\HTTP\HTTP_CONTINUE',                               100);
define('Skyfaring\Simple\HTTP\HTTP_SWITCHING_PROTOCOLS',                    101);
define('Skyfaring\Simple\HTTP\HTTP_STALE_RESPONSE',                         110);
define('Skyfaring\Simple\HTTP\HTTP_REVALIDATION_FAILED',                    111);
define('Skyfaring\Simple\HTTP\HTTP_DISCONNECTED_OPERATION',                 112);
define('Skyfaring\Simple\HTTP\HTTP_HEURISTIC_EXPIRATION',                   113);
define('Skyfaring\Simple\HTTP\HTTP_MISCELLANEOUS_WARNING',                  199);

# Success
define('Skyfaring\Simple\HTTP\HTTP_OK',                                     200);
define('Skyfaring\Simple\HTTP\HTTP_CREATED',                                201);
define('Skyfaring\Simple\HTTP\HTTP_ACCEPTED',                               202);
define('Skyfaring\Simple\HTTP\HTTP_NON_AUTHORITATIVE',                      203);
define('Skyfaring\Simple\HTTP\HTTP_NO_CONTENT',                             204);
define('Skyfaring\Simple\HTTP\HTTP_RESET_CONTENT',                          205);
define('Skyfaring\Simple\HTTP\HTTP_PARTIAL_CONTENT',                        206);
define('Skyfaring\Simple\HTTP\HTTP_TRANSFORMATION_APPLIED',                 214);
define('Skyfaring\Simple\HTTP\HTTP_MISCELLANEOUS_PERSISTENT_WARNING',       299);

# Redirection
define('Skyfaring\Simple\HTTP\HTTP_MULTIPLE_CHOICES',                       300);
define('Skyfaring\Simple\HTTP\HTTP_MOVED_PERMANENTLY',                      301);
define('Skyfaring\Simple\HTTP\HTTP_FOUND',                                  302);
define('Skyfaring\Simple\HTTP\HTTP_SEE_OTHER',                              303);
define('Skyfaring\Simple\HTTP\HTTP_NOT_MODIFIED',                           304);
define('Skyfaring\Simple\HTTP\HTTP_USE_PROXY',                              305);
define('Skyfaring\Simple\HTTP\HTTP_TEMPORARY_REDIRECT',                     307);

# Client error
define('Skyfaring\Simple\HTTP\HTTP_BAD_REQUEST',                            400);
define('Skyfaring\Simple\HTTP\HTTP_UNAUTHORIZED',                           401);
define('Skyfaring\Simple\HTTP\HTTP_PAYMENT_REQUIRED',                       402); // Not implemented
define('Skyfaring\Simple\HTTP\HTTP_FORBIDEN',                               403);
define('Skyfaring\Simple\HTTP\HTTP_NOT_FOUND',                              404);
define('Skyfaring\Simple\HTTP\HTTP_METHOD_NOT_ALLOWED',                     405);
define('Skyfaring\Simple\HTTP\HTTP_NOT_ACCEPTABLE',                         406);
define('Skyfaring\Simple\HTTP\HTTP_PROXY_AUTHENTICATION_REQUIRED',          407);
define('Skyfaring\Simple\HTTP\HTTP_REQUEST_TIMEOUT',                        408);
define('Skyfaring\Simple\HTTP\HTTP_CONFLICT',                               409);
define('Skyfaring\Simple\HTTP\HTTP_GONE',                                   410);
define('Skyfaring\Simple\HTTP\HTTP_LENGTH_REQUIRED',                        411);
define('Skyfaring\Simple\HTTP\HTTP_PRECONDITION_FAILED',                    412);
define('Skyfaring\Simple\HTTP\HTTP_REQUEST_ENTITY_TOO_LARGE',               413);
define('Skyfaring\Simple\HTTP\HTTP_REQUEST_URI_TOO_LONG',                   414);
define('Skyfaring\Simple\HTTP\HTTP_UNSUPPORTED_MEDIA_TYPE',                 415);
define('Skyfaring\Simple\HTTP\HTTP_REQUESTED_RANGE_NOT_SATISFIABLE',        416);
define('Skyfaring\Simple\HTTP\HTTP_EXPECTATION_FAILED',                     417);
define('Skyfaring\Simple\HTTP\HTTP_IM_A_TEAPOT',                            418); // lol

# Server error
define('Skyfaring\Simple\HTTP\HTTP_INTERNAL_SERVER_ERROR',                  500);
define('Skyfaring\Simple\HTTP\HTTP_NOT_IMPLEMENTED',                        501);
define('Skyfaring\Simple\HTTP\HTTP_BAD_GATEWAY',                            502);
define('Skyfaring\Simple\HTTP\HTTP_SERVICE_UNAVAILABLE',                    503);
define('Skyfaring\Simple\HTTP\HTTP_GATEWAY_TIMEOUT',                        504);
define('Skyfaring\Simple\HTTP\HTTP_VERSION_NOT_SUPPORTED',                  505);

# ============================================================================ #
# HTTP STATUS NUMER => LINE MAP
# ============================================================================ #

define('Skyfaring\Simple\HTTP\HTTP_STATUS_MAP', serialize( array(
    '100'   => 'Continue',
    '101'   => 'Switching Protocols',
    '110'   => 'Stale Response',
    '111'   => 'Revalidation failed',
    '112'   => 'Disconnected operation',
    '113'   => 'Heuristic expiration',
    '119'   => 'Miscellaneous warning',
    '200'   => 'Ok',
    '201'   => 'Created',
    '202'   => 'Accepted',
    '203'   => 'Non-authoritative',
    '204'   => 'No content',
    '205'   => 'Reset content',
    '206'   => 'Partial content',
    '299'   => 'Miscellaneous persistent warning',
    '300'   => 'Multiple choices',
    '301'   => 'Moved permanently',
    '302'   => 'Found',
    '303'   => 'See other',
    '304'   => 'Not modified',
    '305'   => 'Use proxy',
    '307'   => 'Temporary redirect',
    '400'   => 'Bad request',
    '401'   => 'Unauthorized',
    '402'   => 'Payment required',
    '403'   => 'Forbidden',
    '404'   => 'Not found',
    '405'   => 'Method not allowed',
    '406'   => 'Not acceptable',
    '407'   => 'Proxy authentication required',
    '408'   => 'Timeout',
    '409'   => 'Conflict',
    '410'   => 'Gone',
    '411'   => 'Length required',
    '412'   => 'Precondition failed',
    '413'   => 'Request entity too large',
    '414'   => 'Request uri too long',
    '415'   => 'Unsupported media type',
    '416'   => 'Requested range not satisfiable',
    '417'   => 'Expectation failed',
    '418'   => 'I\'m a teapot',
    '500'   => 'Internal server error',
    '501'   => 'Not implemented',
    '502'   => 'Bad gateway',
    '503'   => 'Service unavailable',
    '504'   => 'Gateway timeout',
    '505'   => 'Version not supported'
)));

# ============================================================================ #
# HTTP METHODS
# ============================================================================ #

define('Skyfaring\Simple\HTTP\HTTP_METHOD_GET',                          'GET');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_HEAD',                        'HEAD');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_POST',                        'POST');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_PUT',                          'PUT');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_DELETE',                    'DELETE');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_CONNECT',                  'CONNECT');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_OPTIONS',                  'OPTIONS');
define('Skyfaring\Simple\HTTP\HTTP_METHOD_TRACE',                      'TRACE');
