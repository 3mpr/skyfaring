<?php

namespace Skyfaring\Simple\Routing;

use Skyfaring\Simple as S;

/**
 * Simple application controller realized with :
 * 'An Introduction to the Front Controller Pattern, Part 1.'
 * by 'Alejandro Gervasio' on 'sitepoint'.
 *
 * A simple class whose purpose is to redirect the user toward a specific
 * controller. The automatic behaviour is to choose the application behaviour
 * based on the URI ~ FQDN/CONTROLLER/ACTION/ARGS[].
 *
 * Specific behaviours can be altered through the use of setters methods.
 *
 * @author eka
 */
class Router
{
    const ROUTE_DEFINITION_FILE = CONFIG_ROOT.'/routes.yml';

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected $_routesDefinition = null;

    /**
     * @var Crossroads
     */
    protected $_routes = null;

    /**
     * Class constructor.
     *
     * Takes a yaml filepath (if any) as parameter and defines from its content
     * the different EXPLICIT routes.
     *
     * @param string $filepath The route yaml configuration filepath
     *
     * @throws \InvalidArgumentException for inappropriate file
     */
    public function __construct()
    {
        $this->_routes = new Crossroads();
        $conf = S\App::getConfiguration()->get('routes')->toArray();
        $this->_routesDefinition = $conf['routes'];

        $defaults = $this->_routesDefinition['default']['route'];
        foreach ($this->_routesDefinition as $routeName => $routeArray)
        {
            if(empty($routeArray['route'])) continue 1;

            $parameters = array_replace($defaults, $routeArray['route']);

            $name = $routeName;
            $path = $parameters['path'];
            $controller = $parameters['controller'];
            $action = $parameters['action'];
            $methods = $parameters['methods'];

            $this->_routes->add($name, new Route($name, $path, $controller, $action, $methods));
        }
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Tiny creation factory. For chaining.
     *
     * @param string $filepath The route yaml configuration filepath
     */
    public static function create($filepath = self::ROUTE_DEFINITION_FILE)
    {
        return new static($filepath);
    }

    /**
     * Routes getter.
     *
     * @return Crossroads
     */
    public function getRoutes()
    {
        return $this->_routes;
    }

    /**
     * Checks wheter or not the given path has a corresponding route.
     *
     * The expected path format is /controller/action/params whereas params
     * will be stripped.
     *
     * @param string $path The path to be found
     *
     * @return mixed The route name OR false on failure
     */
    protected function match($path)
    {
        return $this->_routes->seek($path);
    }

    /**
     * Finds a route correlating to the client's request and run it.
     *
     * @throws SimpleException for unregistereds routes
     */
    public function route()
    {
        $path = S\App::getRequest()->getUri()->getPath();
        $method = S\App::getRequest()->getMethod();

        if (false == $route = $this->match($path)) {
            throw new S\SimpleException(
                'Undefined route '.$path,
                1002
            );
        } else {
            $this->_routes->get($route)->run($method);
        }
    }
}
