<?php
namespace Skyfaring\Simple\Templating;

class Design
{
    const CSS_ROOT          = '../../public/css';
    const TEMPLATE_ROOT     = SIMPLE_ROOT . '/templates';

    /**
     * Returns a Component instance.
     *
     * @see Component
     */
    static public function component($file, $args = false, $path = null)
    {
        return new Component($file, $args, $path);
    }

    /**
     * Returns a ComponentSet instance based on the given template.
     *
     * @see ComponentSet
     */
    static public function componentSet(Component $template)
    {
        return new ComponentSet($template);
    }

    /**
     * Returns a ComponentSet instance based on the CSS template.
     *
     * @see ComponentSet
     */
    static public function cssSet(\Traversable $cssConf = null)
    {
        if(empty($cssConf)) $cssConf = array();

        $css       = self::component('css.template.html');
        $cssSet    = self::componentSet($css);

        foreach ($cssConf as $file)
        {
            $cssSet->add(array('FILE' => (string) $file));
        }

        return $cssSet;
    }

    /**
     * Returns a Document based on the given array.
     * The array SHOULD respect the views.yml format.
     *
     * @see Document
     */
    static public function document(\ArrayAccess $specification)
    {
        $settings['title']        = $specification['title'];
        $settings['charset']      = $specification['charset'];
        $settings['description']  = $specification['description'];

        $css = self::cssSet($specification['css']);
        // $js     = self::getJsSet($yaml['js']); TODO Implement

        $content = array();
        foreach ($specification['file'] as $name => $file)
        {
            $content[$name] = self::component($file);
        }

        return new Document($settings, $css, null, $content);
    }
}
