<?php

namespace Skyfaring\Simple\Templating;

use Skyfaring\Simple\Stream as SimpleStream;

/**
 * This class describes a list of Component.
 *
 * For optimisation purposes, it uses the __clone() Component method, which
 * means that every Component within this structure shares the same system file.
 *
 * @see Skyfaring\Simple\Templating\Component
 * @see Skyfaring\Simple\Stream\Stream
 */
class ComponentSet implements Template
{
    public static $_separator = "\n";

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

     /**
      * @var Component
      */
     protected $_model = null;

    /**
     * @var array
     */
    protected $_bag = [];

    /**
     * Class constructor.
     *
     * Takes a template as parameter and defines it as the 'root' of this
     * ComponentSet object.
     *
     * @param Component
     */
    public function __construct(Component $template)
    {
        $this->_model = $template;
    }

    /**
     * {@inheritdoc}
     */
    public function __tostring()
    {
        $stream = SimpleStream\StreamFactory::getMemoryStream();
        foreach ($this->_bag as $component) {
            $stream->write((string) $component);
        }

        $content = $stream->getContents();
        $stream->close();
        return $content;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Add a Component to this ComponentSet from with a unique value via 
     * the __clone Component magic method and the given $key-value array.
     *
     * The given SHOULD respect the root component {VARIABLES} logic, any
     * unknown key-value array property will be ignored at rendering.
     *
     * @param array $kvstore The new Component {VARIABLES}
     */
    public function add($key, $value)
    {
        $temp = clone $this->_model;
        $temp->set($key, $value);
        $this->_bag[] = $temp;

        return $this;
    }

    /**
     * Add a Component to this ComponentSet from an array via the __clone
     * Component magic method and the given $key-value array.
     *
     * The given SHOULD respect the root component {VARIABLES} logic, any
     * unknown key-value array property will be ignored at rendering.
     *
     * @param array $kvstore The new Component {VARIABLES}
     */
    public function addArray($array)
    {
        $temp = clone $this->_model;
        foreach($array as $k => $v)
        {
            $temp->set($k, $v);
        }
        $this->_bag[] = $temp;

        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * Creates a new temporary stream and fill its content with every $_bag
     * Component content.
     *
     * @return Stream
     */
    public function compose()
    {
        $stream = SimpleStream\StreamFactory::getMemoryStream();
        foreach ($this->_bag as $component)
        {
            $stream->write($component->render().self::$_separator);
        }

        return $stream;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        return $this->compose()->getContents();
    }

    /**
     * {@inheritdoc}
     */
    public function reset()
    {
        foreach ($this->_bag as $component) {
            $component->reset();
        }

        return $this;
    }
}
