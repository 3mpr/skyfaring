<?php

namespace Skyfaring\Simple\Templating;

use Skyfaring\Simple as S;

class Document implements Template
{
    /**
     * @var Component
     */
    protected static $_skeleton = null;
    
    /**
     * @var array
     */
    protected static $_traits = array(
        'CHARSET', 'TITLE', 'DESCRIPTION'
    );

    /**
     * @var array
     */
    protected static $_habits = array(
        'css' => 'FILE'#, 'js' TODO
    );

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var Directive
     */
    protected $_specification = null;

    /**
     * @var Component
     */
    protected $_render = null;

    /**
     * @var Component[]
     */
    protected $_design = null;

    /**
     * @var string
     */
    protected $_active = null;

    /**
     * Class constructor.
     *
     * Takes a Configuration position ~ Directive reference ~ as parameter.
     *
     * @param Directive The directive reference 
     */
    public function __construct(S\Data\Config\Directive $directive)
    {
        self::initialize();

        $this->_specification = new S\Data\Config\Config($directive);
        $this->_render = clone self::$_skeleton;

        foreach(self::$_traits as $trait)
        {
            $holder = $this->_specification->get($trait);
            if(!is_string($holder)) $holder = $holder->toString();
            $this->_render->set($trait, $holder);
        }

        foreach(self::$_habits as $name => $habit)
        {
            $habitPlan  = new Component($name.'.template.html');
            $habitPlans = new ComponentSet($habitPlan);

            foreach($this->_specification->get($name)->toArray()[$name] as $specifiedHabit)
            {
                $habitPlans->add($habit, $specifiedHabit);
            }
            $this->_render->set($name, $habitPlans);
        }

        foreach($this->_specification->get('file') as $design)
        {
            $this->_design[$design->getName()] = new Component($design);
        }
        
        reset($this->_design);
        $this->_active = key($this->_design);
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->_render->render();
    }

    /**
     * Loads the static $_squeletton Template if not loaded.
     */
    protected static function initialize()
    {
        if (is_null(self::$_skeleton))
        {
            self::$_skeleton = new Component('html.template.html');
            self::$_skeleton->set('LANG', 'en'); // TODO - Dynamise
        }
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Sets this document specification.
     *
     * @param Directive The specification
     */
    public function setSpecificarion(S\Data\Config\Directive $specification)
    {
        $this->_specification = $specification;
    }

    /**
     * Retreives this document specification.
     *
     * @return Directive The specification
     */
    public function getSpecificarion()
    {
        return $this->_specification;
    }

    /**
     * Adds a design to this document possible designs.
     *
     * @param string $name The requested design to add
     */
    public function addDesign($name, Template $design)
    {
        $this->_design[$name] = $design;
    }

    /**
     * Adds a component to this webpage components.
     */
    public function addComponent($name, Component $component)
    {
        $this->_render->set($name, $component);
    }

    /**
     * Removes a design from this document possible designs.
     *
     * @param string $name The requested design to remove
     */
    public function removeDesign($name)
    {
        if (in_array($name, $this->_design))
        {
            unset($this->_design[$name]);
        }

        else
        {
            throw \LogicException(
                'Cannot remove design '.$name.' as it does not exist in document.',
                1402
            );
        }
    }

    /**
     * Retreives a design.
     *
     * @param string $name The requested design name if any
     */
    public function getDesign($name = null)
    {
        if (empty($name)) $name = $this->_active;
        return @$this->_design[$name];
    }

    /**
     * Returns the design array.
     *
     * @return array The designs
     */
    public function getDesigns()
    {
        return $this->_design;
    }

    /**
     * Sets the current active content.
     * 
     * @param string $name The requested active design
     */
    public function draft($name = null)
    {
        if(empty($design))
        {
            reset($this->_design);
            $this->_active = key($this->_design);
            return;
        }

        if (in_array($name, $this->_design))
        {
            $this->_active = $name;
        }

        else
        {
            throw \LogicException(
                'Cannot activate design '.$name.' as it does not exist in document.',
                1403
            );
        }
    }

/* ------------------------------------------------ \Template Implementation */

    /**
     * {@inheritdoc}
     */
    public function reset()
    {
        $this->_render->reset();
    }

    /**
     * {@inheritdoc}
     */
    public function compose()
    {
        $this->_render->set('content', $this->_design[$this->_active]);
        return $this->_render->compose();
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        return $this->compose()->getContents();
    }
}