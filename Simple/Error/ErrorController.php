<?php

namespace Skyfaring\Simple\Error;

use Skyfaring\Simple\MVC\Controller as SController;
use Skyfaring\Simple\MVC\Model as SModel;
use Skyfaring\Simple\MVC\View as SView;

class ErrorController extends SController\Controller
{
    const ENV_DEV = 0;
    const ENV_PROD = 1;

    protected static $_errorMap = null;
    protected static $_errorMsg = null;

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var int
     */
    protected $_errorCount;

    /**
     * @var int
     */
    protected $_environment;

    // TODO - logger

    /**
     * Class constructor.
     *
     * Defines from the given variable the way we display errors to the client,
     * wheter we display a gentle error or the complete exception code & stacktrace.
     *
     * @param int $environment
     */
    public function __construct($environment = ENV)
    {
        self::initialize();

        $this->_errorCount = 0;
        $this->_environment = $environment;

        parent::__construct(null, new SView\View('error'));
    }

    /**
     * Initializes class-wide variables.
     */
    public static function initialize()
    {
        if (empty(self::$_errorMap) || empty(self::$_errorMsg))
        {
            self::$_errorMap = unserialize(ERROR_MAP);
            self::$_errorMsg = unserialize(ERROR_MSG);
        }
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Decides upon this object environment variable what to do (display / log /
     * ...) with the given exception.
     *
     * @param \Exception $e The exception
     */
    public function handle(\Exception $e)
    {
        ++$this->_errorCount;

        list(
            $error,
            $message,
            $stacktrace,
            $httpStatus,
            $headers
        ) = array_pad($this->reveal($e), 5, null);

        $this->_view->add(array(
            'ERROR' => $error,
            'MESSAGE' => $message,
            'STACKTRACE' => $stacktrace,
        ), ENV == self::ENV_PROD ? 'prod' : 'dev');

        $this->respond($this->_view->compose(), $headers, $httpStatus);

        exit(1);
    }

    /**
     * Returns HTTP & HTML aggregated components from an Exception.
     *
     * @param \Exception $e The exception
     *
     * @return array
     */
    protected function reveal(\Exception $e)
    {
        $http = @self::$_errorMap[$e->getCode()];
        
        // Error code not implemented
        if(empty($http))
        {
            $http = '500';
            // TODO - log fallback
        }

        if (ENV == self::ENV_PROD) {
            $err = self::$_errorMap[$e->getCode()];
            $msg = self::$_errorMsg[$err];
            $stacktrace = '';
        } else {
            $err = (string) $e->getCode();
            $msg = $e->getMessage();
            $stacktrace = $this->stackTrace($e->getTrace());
        }

        return array($err, $msg, $stacktrace, $http, array());
    }

    /**
     * Takes a stacktrace as is from an Exception and returns a formatted string.
     *
     * @param array $stack The $exception->getTrace() result
     *
     * @return string The resulting - formatted - string
     */
    protected function stackTrace(array $stack)
    {
        $output = '<b>Stack trace:</b><br><br>'.PHP_EOL;

        $stackLen = count($stack);
        for ($i = 0; $i < $stackLen; ++$i) {
            $entry = $stack[$i];
            $class = empty($entry['class']) ? 'FUNC' : $entry['class'];

            $func = $entry['function'].'(';
            $argsLen = count($entry['args']);
            for ($j = 0; $j < $argsLen; ++$j)
            {
                if(!is_string($entry['args'][$j]))
                {
                    $func .= 'Object';
                }

                else
                {
                    $func .= $entry['args'][$j];
                }

                if ($j < $argsLen - 1)
                {
                    $func .= ', ';
                }
            }
            $func .= ')';

            $output .= '#'.($i).' '
                    .(empty($entry['file'])?' ':$entry['file'].':'.$entry['line'])
                    .' '.$class.'->'.$func.'<br>'.PHP_EOL;
        }

        return $output;
    }

    public function run(array $params = array())
    {
        
    }

    /**
     * Registers this class as the default Exception & Error handler.
     */
    public static function register()
    {
        set_exception_handler(array(new static(), 'handle'));
    }
}
