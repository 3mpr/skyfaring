<?php
namespace Skyfaring\Simple\Stream;

class StreamFactory
{
    static public function getFileStream($path, $mode = Stream::MODE_READ_ONLY)
    {
        return new Stream($path, $mode);
    }

    static public function getMemoryStream()
    {
        return new Stream('php://memory', Stream::MODE_WRITE_READ);
    }

    static public function getHTTPStream($addr, $context)
    {
        $stream = new Stream('http://' . $_SERVER['REMOTE_ADDR']);
    }

    static public function getRequestBody()
    {
        return new Stream('php://input', Stream::MODE_READ_ONLY);
    }
}
