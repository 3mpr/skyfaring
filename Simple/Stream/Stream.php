<?php

namespace Skyfaring\Simple\Stream;

use Skyfaring\Simple\PSR as PSR;

class Stream implements PSR\StreamInterface
{
    /**
     * These constants reference standard file interaction behaviours.
     *
     * @see http://php.net/manual/en/function.fopen.php
     */
    const MODE_READ_ONLY = 'r';
    const MODE_READ_WRITE = 'r+';

    const MODE_WRITE_ONLY = 'w'; // Delete existing content
    const MODE_WRITE_READ = 'w+'; // Delete existing content

    const MODE_WRITE_CONCAT = 'a';
    const MODE_READ_WRITE_CONCAT = 'a+';

    const MODE_CREATE_WRITE = 'x';
    const MODE_CREATE_READ_WRITE = 'x+';

    const MODE_WRITE_ONLY_NO_TRUNCATE = 'c'; // Does not fail
    const MODE_READ_WRITE_NO_TRUNCATE = 'c+'; // Does not fail

    public static $modes = ['r', 'r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+'];
    public static $modes_write = ['r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+'];
    public static $modes_read = ['r', 'r+', 'w+', 'a+', 'x+', 'c+'];

    const FILE_MAX_LINE_LENGTH = 4096;

/* ========================================================================== */
/* === VARIABLES & MAGIC ==================================================== */
/* ========================================================================== */

    /**
     * @var array
     */
    protected $_context;

    /**
     * @var string
     */
    protected $_path;

    /**
     * @var mode
     */
    protected $_mode;

    /**
     * @var resource
     */
    protected $_file;

    /**
     * @var bool
     */
    protected $_open = false;

    /**
     * Class constructor.
     *
     * Takes the file path and opening mode as parameters.
     *
     * @param string The file path
     * @param string The file opening mode
     *
     * @throws \RuntimeException
     */
    public function __construct($path, $mode = self::MODE_READ_ONLY)
    {
        $this->_path = (string) $path;

        if (in_array($mode, self::$modes))
        {
            $this->_mode = $mode;
        } 
        
        else
        {
            throw new \RuntimeException(
                'Specified mode ['.(string) $mode.'] is not supported.',
                1301
            );
        }

        $this->_file = @fopen($this->_path, $this->_mode);

        if (false === $this->_file)
        {
            throw new \RuntimeException(
                'An error occured while trying to open ['.$this->_path.'].',
                1302
            );
        } 
        
        else
        {
            $this->_open = true;
        }
    }

    /**
     * Class destructor.
     *
     * Close the object file descriptor.
     */
    public function __destruct()
    {
        if ($this->_open) {
            @fclose($this->_file);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        $has_to_be_closed = false;

        if (!$this->_open) {
            $this->_file = @fopen($this->_path, self::MODE_READ_ONLY);

            if (false === $this->_file) {
                return 'Error: unable to open file\n';
            }

            $this->_open = true;
            $has_to_be_closed = true;
        }

        $string = stream_get_contents($this->_file, -1, 0);

        if (false === $string) {
            return 'Error: unexpected error while reading file\n';
        }

        if ($has_to_be_closed) {
            @fclose($this->_file);
        }

        return $string;
    }

/* ========================================================================== */
/* === METHOD IMPLEMENTATIONS =============================================== */
/* ========================================================================== */

    /**
     * Factory constructor for chaining purposes.
     *
     * @param string The file path
     * @param string The file opening mode
     *
     * @see __construct
     */
    public static function create($path, $mode = self::MODE_READ_ONLY)
    {
        return new static($path, $mode);
    }

    /**
     * {@inheritdoc}
     */
    public function close() // ?????
    {
        // if ($this->_open) {
        //     @fclose($this->_file);
        //     $this->_open = false;
        // }
    }

    /**
     * {@inheritdoc}
     *
     * Does not make much sense for a file.
     */
    public function detach()
    {
        $this->close();

        return $this->_file;
    }

    /**
     * {@inheritdoc}
     */
    public function getSize()
    {
        if ($this->_open) {
            return filesize($this->_path);
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function tell()
    {
        if ($this->_open) {
            return ftell($this->_file);
        } else {
            return -1;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function eof()
    {
        if ($this->_open) {
            return feof($this->_file);
        } else {
            return -1;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isSeekable()
    {
        return ($this->_open) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if (!$this->isSeekable()) {
            throw new \RuntimeException(
                'Seek command on closed file.',
                1303
            );
        }

        if (-1 === fseek($this->_file, $offset, $whence)) {
            throw new \RuntimeException(
                'Unable to displace file pointer on ['.$this->_path.'].',
                1304
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        if (false === rewind($this->_file)) {
            throw new \RuntimeException(
                'Rewind on file ['.$this->_path.'] did not succeed.',
                1305
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isWritable()
    {
        if ($this->_open) {
            return in_array($this->_mode, self::$modes_write);
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function write($string)
    {
        if (!$this->isWritable() || !fwrite($this->_file, (string) $string)) {
            throw new \RuntimeException(
                'Unable to write to file ['.$this->_path.'].',
                1306
            );
        }
    }

    /**
     * Truncate the file to zero length and write new content.
     *
     * @param string $string Replacing string
     *
     * @throws \RuntimeException on failure
     */
    public function overwrite($string)
    {
        if (!$this->isWritable()) {
            throw new \RuntimeException(
                'File ['.$this->_path.'] is not writable.',
                1307
            );
        }

        if (!ftruncate($this->_file, 0)) {
            throw new \RuntimeException(
                'Unable to truncate file ['.$this->_path.'].',
                1308
            );
        }

        $this->write($string);
    }

    /**
     * {@inheritdoc}
     */
    public function isReadable()
    {
        if ($this->_open)
        {
            return in_array($this->_mode, self::$modes_read);
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function read($length)
    {
        if (!$this->isReadable()
        || false === $retval = fread($this->_file, $length))
        {
            throw new \RuntimeException(
                'Unable to read file ['.$this->_path.']',
                1309
            );
        }

        return $retval;
    }

    /**
     * {@inheritdoc}
     */
    public function getContents()
    {
        if (!$this->_open)
        {
            if (!$this->isReadable())
            {
                throw new \RuntimeException(
                    'Unable to read file ['.$this->_path.'].<br>'.PHP_EOL
                    .'File state is : '.($this->_open ? 'Opened' : 'Closed'),
                    1309
                );
            }
        }

        return stream_get_contents($this->_file, -1, 0);
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata($key = null)
    {
        if (!$this->_open) {
            return null;
        }

        $retval = stream_get_meta_data($this->_file);

        if (null === $key) {
            return $retval;
        }

        return $retval[$key];
    }
}
