USE skyfaring;

DELETE FROM Types;

INSERT INTO Types ( name )
VALUES 	( `administrator` ),
		( `user` ),
        ( `moderator` ),
        ( `pending` );
