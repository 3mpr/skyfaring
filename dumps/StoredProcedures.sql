USE skyfaring;

DROP PROCEDURE IF EXISTS `getUser`;
DROP PROCEDURE IF EXISTS `getUserByName`;
DROP PROCEDURE IF EXISTS `setUser`;
DROP PROCEDURE IF EXISTS `deleteUser`;

DELIMITER //

##
# Generic user select query.
##
CREATE PROCEDURE `getUser` (
	IN id 		    int(10),
	OUT username	varchar(30),
	OUT password	varchar(70),
	OUT email		varchar(255),
	OUT act_code	varchar(45),
	OUT activated	boolean,
	OUT disabled	boolean,
	OUT name		varchar(45))
BEGIN
	SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	START TRANSACTION;
		SELECT Users.username, Users.password, Users.email, Users.act_code,
		Users.activated, Users.disabled, Type.name FROM Users, Type
		INNER JOIN UserTypes ON Users.id = UserTypes.user_id
		INNER JOIN Types ON UserTypes.type_id = Types.id
		WHERE Users.id = id;
	COMMIT;
END//

##
# Generic user BY NAME select query.
##
CREATE PROCEDURE `getUserByName` (
	IN username 	varchar(30),
	OUT id	        int(10),
	OUT password	varchar(70),
	OUT email		varchar(255),
	OUT act_code	varchar(45),
	OUT activated	boolean,
	OUT disabled	boolean,
	OUT name		varchar(45))
BEGIN
	SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	START TRANSACTION;
		SELECT Users.id, Users.password, Users.email, Users.act_code,
		Users.activated, Users.disabled, Types.name FROM Users
		INNER JOIN UserTypes ON Users.id = UserTypes.user_id
		INNER JOIN Types ON UserTypes.type_id = Types.id
		WHERE Users.username = username;
	COMMIT;
END//

##
# Generic user insert query.
##
CREATE PROCEDURE `setUser` (
	IN username	varchar(30),
	IN password	varchar(70),
	IN email	varchar(255),
	IN act_code	varchar(45),
	IN type_id	int(4),
	OUT id		int(10))
BEGIN
	SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	START TRANSACTION;
		INSERT INTO Users ( `username`, `password`, `email`, `act_code` )
		VALUES (username, password, email, act_code);
		INSERT INTO UserTypes ( `user_id`, `type_id` )
		VALUES (LAST_INSERT_ID(), type_id);
		SELECT LAST_INSERT_ID() AS id;
	COMMIT;
END//

##
# Generic delete user query.
##
CREATE PROCEDURE `deleteUser` ( IN id int(10) )
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    START TRANSACTION;
        SET FOREIGN_KEY_CHECKS = 0;
        DELETE FROM UserTypes WHERE user_id = `id` LIMIT 1;
        DELETE FROM Users WHERE id = `id` LIMIT 1;
        SET FOREIGN_KEY_CHECKS = 1;
    COMMIT;
END//


DELIMITER ;
