CREATE TABLE IF NOT EXISTS `Types` (
	`id`					int(4)		unsigned		NOT NULL	auto_increment,
	`name`				varchar(45)						NOT NULL,
	PRIMARY KEY		(`id`)
);

CREATE TABLE IF NOT EXISTS `Users` (
  `id` 					int(10)			unsigned 		NOT NULL 	auto_increment,
  `username` 		varchar(30) 						NOT NULL    UNIQUE,
  `password` 		varchar(70) 						NOT NULL,
  `email` 			varchar(255)						NOT NULL    UNIQUE,
  `act_code` 		varchar(45) 						NOT NULL,
  `activated`		boolean								NOT NULL	default FALSE,
  `disabled`		boolean								NOT NULL	default FALSE,
  PRIMARY KEY  	(`id`),
);

CREATE TABLE IF NOT EXISTS `UserTypes` (
	(`user_id`) 	int(10)				unsigned		NOT NULL,
	(`type_id`)		int(4)				unsigned		NOT NULL,
	PRIMARY KEY 	(`user_id`),
	PRIMARY KEY 	(`type_id`),
	CONSTRAINT 		fk_user				FOREIGN KEY	(`user_id`)	REFERENCES Users(`id`),
	CONSTRAINT 		fk_type				FOREIGN KEY (`type_id`) REFERENCES Types(`id`)
);
