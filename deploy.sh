#!/bin/bash

sass Simple/assets/css/skf.scss > Simple/assets/css/skf.css

rm -rf docker/app/Skyfaring/*

rsync -a public docker/app/Skyfaring
rsync -a Simple docker/app/Skyfaring
cp .htaccess docker/app/.htaccess
