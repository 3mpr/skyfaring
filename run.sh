#!/bin/bash

# Docker development environment creation script.

# Deletes container & docker directories if present
docker stop skyfaring 2> /dev/null
docker rm skyfaring 2> /dev/null
[[ -d docker ]] && rm -rf docker/*

# Start the container without volumes
docker run -itd --name skyfaring \
 -p 80:80 -p 443:443 \
 skyfaring:latest

# Copy directories from the container to the host and rename those
mkdir -p docker/app && mkdir docker/etc && mkdir docker/log

docker cp skyfaring:/app docker/

docker cp skyfaring:/etc/apache2 docker/ && mv docker/apache2 docker/etc \
&& mv docker/etc/apache2/* docker/etc/ && rmdir docker/etc/apache2

docker cp skyfaring:/var/log/apache2 docker/ && mv docker/apache2 docker/log \
&& mv docker/log/apache2/* docker/log/ && rmdir docker/log/apache2

# Stops the container, deletes it and recreates it with volumes
docker stop skyfaring
docker rm skyfaring

docker run -itd --name skyfaring \
 -p 80:80 -p 443:443 \
 -v "$PWD/docker/etc":"/etc/apache2" \
 -v "$PWD/docker/log":"/var/log/apache2" \
 -v "$PWD/docker/app":"/app" \
 skyfaring:latest
