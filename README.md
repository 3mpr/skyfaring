Small PHP foundation covering various Object Oriented Programming principles such as Routing, MVC, ORM so on so forth.    
Code guidelines can be found [here](http://www.php-fig.org/).    

# Install #
- - -
### With **Docker** : ###
```shell
$ git clone https://SpaceHacker@bitbucket.org/SpaceHacker/skyfaring.git
$ docker build -t skyfaring .
$ ./run.sh
```    
    
Skyfaring is now available at ```http://localhost:80```

### Without **Docker** : ###
Verify your httpd installation and (depending on your document root folder) :
```
#!bash
$ cd /document/root/folder
$ git clone https://SpaceHacker@bitbucket.org/SpaceHacker/skyfaring.git
```
You will want to modify the **Autoloader.php** class at line 28 to reflect your document root :
```
#!php
self::$pathTop = '/your/document/root/folder';
```
A sample virtual host configuration file *skyfaring.conf* is available.    
The virtual host needs either to activate **modrewrite** and redirect requests toward *public/index.php* or to allow the given .htaccess to override the virtual host rules.

# Contribute #
- - -
Switch to the current branch (*Peppermint as of 10/10/2016*) and... well, you're pretty much free to add / fix any issue you'd come across from here.
```
#!bash
git checkout peppermint
```
After developing a bit, you may want to see your changes.    
To do so just use **deploy.sh**, the script will push your changes to the container */app* volume.

# TODO #
- - -
Any of the following points are to be considered for development **ALTHOUGH** this list does **NOT** intend to stand as a guideline and **IS NOT** exhaustive.    
Again, feel free to contribute where you feel it's needed.    

* Write documentation, lots ;
* Pick a test framework (PHPUnit / Codeception) and write tests, lots ;
* End MVC **base** classes logic ;
* Implement route regex recognition ;
* Create a logger w/ or w/o docker in mind ;
* Pick **OR** create an ORM, nothing huge ;
* Upgrade the Templating system.

- - -

> This project stands *mainly* for studying purposes and **DOES NOT** intend to replace frameworks *'leaders'* (e.g Laravel or Yii) !