FROM frolvlad/alpine-glibc
MAINTAINER Florian Indot <florian.indot@gmail.com>

# Install dependencies
RUN apk add --update php5 php5-dev php5-cli php5-json php5-phar php5-openssl php5-pear \
    php5-apache2 apache2-utils yaml yaml-dev autoconf alpine-sdk \
    && rm -rf /var/cache/apk/*

# PECL script fix
RUN PECL_PATH=$(which pecl) && sed -i 's#exec $PHP -C -n -q $INCARG -d# exec $PHP -C -q $INCARG -d#' $PECL_PATH

# YAML lib installation from PECL
RUN printf "\n" | pecl install -a yaml \
 && echo "extension=yaml.so" > /etc/php5/conf.d/yaml.ini

# Remove now unnecessary packages
RUN apk del autoconf alpine-sdk && rm -rf /var/cache/apk/*

# Apache 2 configuration modification via sed
RUN sed -i 's@#LoadModule rewrite_module modules/mod_rewrite.so@LoadModule rewrite_module modules/mod_rewrite.so@' /etc/apache2/httpd.conf \
    && sed -i 's@^DocumentRoot ".*@DocumentRoot "/app/Skyfaring"@g' /etc/apache2/httpd.conf \
    && sed -i 's@AllowOverride none@AllowOverride All@' /etc/apache2/httpd.conf

# Directories creation
RUN mkdir -p /app/Skyfaring && chown -R apache:apache /app && mkdir /run/apache2 \
    && mkdir /app/Skyfaring/Simple && mkdir /app/Skyfaring/public

# Host to container copies
ADD ["skyfaring.conf", "/etc/apache2/conf.d/"]
ADD ["Simple", "/app/Skyfaring/Simple"]
ADD ["public", "/app/Skyfaring/public"]
ADD [".htaccess", "/app/Skyfaring"]

VOLUME ["/etc/apache2", "/var/log/apache2", "/app"]
EXPOSE 80 443

ENTRYPOINT ["httpd", "-D", "FOREGROUND"]
